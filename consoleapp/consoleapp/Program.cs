﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consoleapp
{
    class Program
    {
        static void Main(string[] args)
        {
            ////Q1 Answer
            Q1 _objq1 = new Q1(); _objq1.operations();
            
            ////Q2 Answer
            //Q2 _objq2 = new Q2(); _objq2.swaptwo_num();

            ////Q3 Answer
            //Q3 _objq3 = new Q3();_objq3.NestedStruct();

            ////Q4 Answer
            //Q4 _objq4 = new Q4();_objq4.number_square();

            ////Q5 ANswer
            //Q5 _objq5 = new Q5();Console.WriteLine(_objq5.findcharacters("apple"));

            ////Q6 Answer
            //Q6 _objq6 = new Q6();_objq6.number_frequency();

            ////Q7 Answer
            //Q7 _objq7 = new Q7();_objq7.createfile();

            ////Q8 Answer
            //int val;Q8 _objq8 = new Q8(); val = 66;Console.Write("Decimal = " + val);Console.Write("\nBinary of {0} = ", val);_objq8.displayBinary(val);Console.ReadLine();Console.Write("\n");

            Console.ReadLine();
        }
    }

    public class Q1
    {
        public void operations()
        {
            Console.WriteLine(-1 + 4 * 6);
            Console.WriteLine((35 + 5) % 7);
            Console.WriteLine(14 + -4 * 6 / 11);
            Console.WriteLine(2 + 15 / 6 * 1 - 7 % 2);
        }
    }

    public class Q2
    {
        public void swaptwo_num()
        {
            int number1, number2, temp;
            Console.Write("\nInput the First Number : ");
            number1 = int.Parse(Console.ReadLine());
            Console.Write("\nInput the Second Number : ");
            number2 = int.Parse(Console.ReadLine());
            temp = number1;
            number1 = number2;
            number2 = temp;
            Console.Write("\nAfter Swapping : ");
            Console.Write("\nFirst Number : " + number1);
            Console.Write("\nSecond Number : " + number2);

        }
    }

    public class Q3
    {
        struct employee
        {
            public string eName;
            public dtObirth Date;
        }
        struct dtObirth
        {
            public int Day;
            public int Month;
            public int Year;
        }
        public void NestedStruct()
        {

            int dd = 0, mm = 0, yy = 0;
            int total = 2;
            Console.Write("\n\nCreate a nested struct and store data in an array :\n");
            Console.Write("-------------------------------------------------------\n");
            employee[] emp = new employee[total];

            for (int i = 0; i < total; i++)
            {
                Console.Write("Name of the employee : ");
                string nm = Console.ReadLine();
                emp[i].eName = nm;

                Console.Write("Input day of the birth : ");
                dd = Convert.ToInt32(Console.ReadLine());
                emp[i].Date.Day = dd;

                Console.Write("Input month of the birth : ");
                mm = Convert.ToInt32(Console.ReadLine());
                emp[i].Date.Month = mm;

                Console.Write("Input year for the birth : ");
                yy = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                emp[i].Date.Year = yy;
            }
        }
    }

    public class Q4
    {
        public void number_square()
        {
            var arr1 = new[] { 9, 8, 6, 5 };

            Console.Write("\nLINQ : Find the number and its square of an array");
            Console.Write("\n------------------------------------------------------------------------\n");

            var sqNo = from int Number in arr1
                       let SqrNo = Number * Number
                       where SqrNo > 20
                       select new { Number, SqrNo };

            foreach (var a in sqNo)
                Console.WriteLine(a);


        }
    }

    public class Q5
    {
        public string findcharacters(string s)
        {
            //string repeatedWord = "apple";
            s = s.ToLower();
            char[] chars = s.ToCharArray();
            Array.Sort(chars);
            StringBuilder sb = new StringBuilder();
            int count = 0;
            for (int i = 0; i < chars.Length; i++)
            {
                if (chars[i] < 'a' || chars[i] > 'z') continue;
                if (sb.Length == 0)
                {
                    sb = sb.Append("Character " + chars[i]);
                    count = 1;
                }
                else if (chars[i] == chars[i - 1])
                {
                    count++;
                }
                else
                {
                    sb = sb.Append(":" + count.ToString() + " time").AppendLine();
                    sb = sb.Append("Character " + chars[i]);
                    count = 1;
                }
            }
            sb = sb.Append(":" + count.ToString() + " time");
            return sb.ToString();
        }
    }

    public class Q6
    {
        public void number_frequency()
        {
            int[] array = new int[] { 5, 1, 9, 2, 3, 7, 4, 5, 6, 8, 7, 6, 3, 4, 5, 2 };
            var dict = new Dictionary<int, int>();
            foreach (var value in array)
            {
                if (dict.ContainsKey(value)) dict[value]++;
                else dict[value] = 1;
            }
            Console.WriteLine("Number | Number * Frequency | Frequency");
            foreach (var pair in dict)
                Console.WriteLine("{0} | {1} | {2}", pair.Key, pair.Key * pair.Value, pair.Value);
        }
    }

    public class Q7
    {
        public void createfile()
        {
            string fileName = @"C:\test.txt";

            try
            {
                // Check if file already exists. If yes, delete it.   
                if (File.Exists(fileName))
                {
                    Console.WriteLine("Dupliacte : File already exist");
                }

                else
                {
                    // Create a new file   
                    using (FileStream fs = File.Create(fileName))
                    {

                        Console.WriteLine("Succesdfull creation : A file created with name test.txt  ");
                        // Add some text to file  
                        Byte[] title = new UTF8Encoding(true).GetBytes("my test content");
                        fs.Write(title, 0, title.Length);
                        byte[] author = new UTF8Encoding(true).GetBytes("this is 1st line");
                        fs.Write(author, 0, author.Length);
                    }

                    // Open the stream and read it back.  
                    using (StreamReader sr = File.OpenText(fileName))
                    {
                        string s = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                            Console.WriteLine(s);
                            Console.Write("\n");
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }
    }

    public class Q8
    {
        public int displayBinary(int dec)
        {
            int res;
            if (dec != 0)
            {
                res = (dec % 2) + 10 * displayBinary(dec / 2);
                Console.Write(res);
                return 0;
            }
            else
            {
                return 0;
            }
        }
    }

    

    

    

    
}


